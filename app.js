const path = require('path');

const express = require('express');

const app = express();

app.use(express.static('public'));

app.get('/', (req, res) => {
  const filePath = path.join(__dirname, 'views', 'welcome.html');
  process.env.STORY_FOLDER
  res.sendFile(filePath);
});

app.get('/env', (req, res) => {
  res.send(`
    <h1>!Hello from this NodeJS app!</h1>
    <p>env text ${process.env.WELCOME_TEXT}</p>
  `);
});

app.get('/error', (req, res) => {
  process.exit(1);
});

app.listen(80);
  